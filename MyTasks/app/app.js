define([], function(){
    var app = {};

    //create and download file from js
    app.download = function download(filename, text) {
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    }

    //add string functionalities
    if(String.prototype.replaceAll == null){
        String.prototype.replaceAll = function(search, replacement) {
            return this.indexOf(search)==-1 ? this : this.split(search).join(replacement);
        };
    }


    return app;
});
