define([], function(){
	var storage = {
		_dt: null,
		get: function(){
			if(this._dt===null){
				this._dt = JSON.parse(localStorage.getItem("data"));
				if(this._dt===null){
					console.log("data not found so created default");
					this._dt = {
						props : {
							curProjName: "+",
							baseDir: "E:/Work/Proj-Data"
						},
						projects : {}
					};
				}
			}
			return this._dt;
		},
		save: function(){
			if(this._dt===null) return false;
			localStorage.setItem("data", JSON.stringify(this._dt));
			return true;
		}
	};

	storage.__defineGetter__("data", function(){
	   return this.get();
	});

	storage.getCurProj = function(){
		var data = storage.get();
		return data.projects[data.props.curProjName];
	};

	storage.getProjArr = function(){
		var data = storage.get();
		return Object.values(data.projects);
	};

	return storage;
});
