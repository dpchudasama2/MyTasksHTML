define(["jquery"], function($){
	function create(method, config){
		var config = config || {};
		return function(arg1){
			if($.isFunction(config.wrapArg))
				arg1 = config.wrapArg.apply(this, arguments);
			console.log("Executing:", method, arg1);
			$.get("http://localhost:8000/fix/cmd?method="+method+"&arg1="+encodeURIComponent(arg1), {
				error: function(){
					if($.isFunction(config.onerr))
						config.onerr(arg1)
				}
			});
		};
	};

	var cmd = {
		browse: create("browse"),
		exec: create("exec", {wrapArg: function(){ //main
			return Array.from(arguments).join('|');
		}}),
		npp: function(arg1){
			return this.exec("C:/Program Files/Notepad++/notepad++.exe", arg1);
		},
		notepad: function(arg1){
			return this.exec("notepad", arg1);
		},
		open: create("open"), //specially for directories, also works for system default opener apply
		edit: create("edit", {onerr: function(arg1){
			console.log("Re trying with other command");
			return this.npp(arg1);
		}})
	};
	
//	cmd.dir = cmd.open;
	cmd.dir = create("open", {onerr: function(arg1){
		var c = 'mkdir "'+arg1+'"';
		if(typeof copy == "function"){
			copy(c);
			console.log('Directory "'+arg1+'" not found, execute auto-copied code in cmd');
		} else {
			console.log('Directory "'+arg1+'" not found, execute following command in cmd');
			console.log(c);
		}

		/*var r = confirm("Directory \""+arg1+"\" not found, do you want to create ?")
		if(r)
			return cmd.exec("mkdir", arg1);*/
	}}),
	cmd.file = cmd.npp;
	return cmd; //or use name system
});
